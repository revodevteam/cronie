<?php

namespace Framework\Cronie;

trait Interval {

    public function allTime()
    {
        $this->on('* * * * * *');
        
        return $this;
    }

    public function hourly()
    {
        $this->on('0 * * * * *');
        
        return $this;
    }

    public function daily()
    {
        $this->on('0 0 * * * *');
        
        return $this;
    }

}
