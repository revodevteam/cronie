<?php

namespace Framework\Cronie;

use DateTime;
use Exception;
use Framework\FileSystem\FileManager;

class Schedule {

    protected $jobs;
    protected $lockFile;
    protected $isLocked;
    protected $timeString;

    public function __construct()
    {
        session_write_close();

        $this->jobs = [];
        $this->lockFile = VAR_DIR . DS . 'locks/cronie.lck';
        //$this->isLocked = $this->isItLocked();

        $this->boot();
    }

    protected function boot()
    {
//        if ($this->isLocked) {
//            exit('Process is locked by another cronie !');
//        }
//
//        $this->lockKyaJaye();
        $this->createTimeString();
    }

    /**
     * 
     * @param string $controller
     * @param string $method
     * @return \Framework\Cronie\Job
     */
    public function call(string $controller, string $method)
    {
        $job = new Job();
        $job->setupControllerAction($controller, $method);

        return $this->jobs[] = $job;
    }

    /**
     * 
     * @param string $route
     * 
     * @return \Framework\Cronie\Job
     */
    public function route(string $route)
    {
        $job = new Job();

        return $this->jobs[] = $job;
    }

    public function run($request)
    {
        if (empty($this->jobs)) {
            throw new Exception('No job Found !');
        }

        foreach ($this->jobs as $job) {
            if (preg_match($this->timeString, $job->ticktock(), $matches)) {

                $job->exec($request);
            }
        }
    }

    protected function createTimeString()
    {
        $dateTime = new DateTime();
        $minutes = ($dateTime->format('i') !== '00') ? ltrim($dateTime->format('i'), '0') : '0';
        $this->timeString = $minutes . ' ' . $dateTime->format('G j n N Y');
        $this->timeString = '~(?:' . preg_replace('#(\d+)#', '(?:$1|\*)', $this->timeString) . ')~';

        return $this;
    }

    /**
     * Check if Cronie is locked by another Cronie
     * @return boolean
     */
    public function isItLocked()
    {
        return (file_exists($this->lockFile) === true) && (filemtime($this->lockFile) > (time() - 3600));
    }

    protected function lockKyaJaye()
    {
        FileManager::checkDIR(dirname($this->lockFile));

        return file_put_contents($this->lockFile, '') !== false;
    }

    protected function unlockKyaJaye()
    {
        FileManager::unlink($this->lockFile);

        return file_exists($this->lockFile) === false;
    }

    public function __destruct()
    {
        //$this->unlockKyaJaye();
    }

}
