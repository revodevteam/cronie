<?php

namespace Framework\Cronie;

class Job {

    use Interval;

    const CONTROLLER_ACTION = 1;
    const ROUTE_CALL = 2;
    const EXEC_FOREGROUND = 1;
    const EXEC_BACKGROUND = 2;

    protected $task;
    protected $ticktock;
    protected $execution_mode;

    public function __construct()
    {
        $this->execution_mode = self::EXEC_FOREGROUND;
    }

    public function setupControllerAction(string $controller, string $method)
    {
        $this->task = (object) ['controller' => $controller, 'method' => $method, 'type' => self::CONTROLLER_ACTION];

        return $this;
    }

    public function setupRouteCall(string $route)
    {
        $this->task = (object) ['route' => $route, 'type' => self::ROUTE_CALL];

        return $this;
    }

    public function background()
    {
        $this->execution_mode = self::EXEC_BACKGROUND;

        return $this;
    }

    public function on(string $timer)
    {
        $this->ticktock = $timer;
    }

    public function ticktock()
    {
        return $this->ticktock;
    }

    public function isControllerAction()
    {
        return $this->task->type === self::CONTROLLER_ACTION;
    }

    public function exec($request)
    {
        if ($this->task->type === self::CONTROLLER_ACTION) {
            if ($this->execution_mode === self::EXEC_FOREGROUND) {
                $controllerName = '\\Controllers\\' . $this->task->controller . 'Controller';
                $controller = new $controllerName();

                $controller->{$this->task->method}($request);
            } else {
                return shell_exec('php ' . ROOT_DIR . '/revocop exec:action ' . $this->task->controller . '::' . $this->task->method . ' > /dev/null 2>/dev/null &');
            }
        } elseif ($this->task->type === self::ROUTE_CALL) {
            
        }
    }

    protected function execRoute()
    {
        
    }

    protected function shellExec()
    {
        //return shell_exec('php '.ROOT_DIR.'/revocop ')
    }

}
